# Modeso Task
----
#Technologies
   -NodeJS: v12.7.0
   
   -MySql: 5.7.26
   
   -Angular 7



#Application Setup
  1- create api/src/config/config.json and set database credentials as shown:
      
      {
        "development": {
          "username": "DATABASE_USERNAME",
          "password": "DATABASE_PASSWORD",
          "database": "DATABASE_NAME",
          "host": "127.0.0.1",
          "dialect": "mysql",
          "define": {
                "charset": "utf8",
                "dialectOptions": {
                  "collate": "latin-1" }
              }
        },
        "test": {
          "username": "DATABASE_USERNAME",
          "password": "DATABASE_PASSWORD",
          "database": "DATABASE_NAME",
          "host": "127.0.0.1",
          "dialect": "mysql",
          "define": {
                "charset": "utf8",
                "dialectOptions": {
                  "collate": "latin-1" }
              }
        },
        "production": {
          "username": "DATABASE_USERNAME",
          "password": "DATABASE_PASSWORD",
          "database": "DATABASE_NAME",
          "host": "127.0.0.1",
          "dialect": "mysql",
          "define": {
                "charset": "utf8",
                "dialectOptions": {
                  "collate": "latin-1" }
              }
        }
      }
  2- create api/src/config/local-env.js for JWT_SECRET:
  
    JWT_SECRET="any JWT secret you want :)";
    
    module.exports = JWT_SECRET;
  3- used packages:
    
    1-bcrypt: 3.0.6 -> to hash passwords
    2-express: 4.16.4 -> application server
    3-jsonwebtoken: 8.5.1 -> use tokens for authentication and authorizations
    4-multer: 1.4.2 -> to upload images
    5-mysql2: 1.6.5 -> to connect to mysql database
    6-sequelize: 5.15.1 -> an orm 
  4- install packages :
  
    run npm install
  5- run migrations:
  
        npm install --save sequelize-cli -> to install sequelize-cli
        npx sequelize-cli init -> To create an empty project you will need to execute init command
        sequelize db:migrate
   Sequelize Docs for more info: 
    https://sequelize.org/master/manual/getting-started.html

  6- use nodemon instead of node to run index.js:
    
    npm install -g nodemon
 7- to start the api server 
        
     1- cd api/
     2- nodemon index.js
----
#Project Structure:
-project Structure :


    api/_
         |_src/
              |_config/
              |_controllers/
              |_middleware/
              |_migrations/
              |_models/
              |_routers/
              |_routers-validators/
              |_seeders/
         |_index.js
         |_package.json
         |_package-lock.json
    frontend/_
              |src
                  |_app
                       |_auth
                       |_components
                       |_note
                       |_user
                       |_app.component.html
                       |_app.component.scss
                       |_app.component.ts
                       |_app.module.ts
                       |_app.routing.ts
                  |_assets
   
   - Project structure explanation :
   
   1- src: will have all api code
   
   2- config: all api configs as db and secrets
   
   3- middleware: application middleware like(authentication/ validating file uploads)
   
   4- migrations: database migrations
   
   5- models: models of database tables
   
   6- routers: models routes 
   
   7- routers-validators: to validate requests parameters 
   
   8- seeders: seeding files
   
   9- index.js: application entry point
   ___
   ##Frontend Structure explanation:
   
   because working in modular way was my interest i decided to make every component as a module to reuse it any time i want
   
   1- app: main app directory which contains app component, module, main routing
   
   2- auth: auth module which contains only authentication related components, services, models, guard and routing as shown
   
    auth
       |_models
       |_sign-in : sign-in component
       |_sign-up : sign-up component 
       |_auth.module.ts : auth module -> contains needed imports
       |_auth.service.ts : auth service which will make HTTP requests
       |_auth.guard.ts  : authorization guard
       |_auth.routing.module.ts : auth module routing (sign-in/sign-up)
   3- components: which contains shared components and shared module to share shared components across modules with same styles 
   
    components
              |_landing : landing page component             
              |_not-found : not found page component
              |_shared
                     |_ button : shared button
                     |_ footer : shared footer
                     |_ input-field : shared input field
                     |_ navbar : shared navbar
                     |_ shared.module.ts : shared module to include all shared components in many modules
                     
   4- note: which contains note components only with auth service injected to get current user to authorize actions
   
    note
       |_models : note model
       |_notes-create : create note component
       |_notes-edit : edit note component
       |_notes-info : show note component
       |_note.module.ts : contains needed imports of note module
       |_note-resolver.service.ts : because we need to fetch data before edit component Oninit we need this resolver to fetch note data before edit component creation which fetches the data while routing      
       |_note-routing.module.ts : note module routing
       |_note-service.ts        : note HTTP requests, shared note methods

  5- user: which contains user components only with auth service injected
   
    user
       |_user-edit-form : edit user data component
       |_user-info      : show user data component     
       |_user-notes     : show user notes (His Home Page)
       |_user.module.ts : contains needed imports of user module 
       |_user.service.ts: user HTTP requests, shared methods
       |_user-resolver.service.ts : because we need to fetch data before edit component Oninit we need this resolver to fetch user data before edit component creation which fetches the data while routing
       |_user-routing.module.ts : user module routing
       
___
### To Start angular server:
    cd  /frontend
    ng serve 
___
#API EndPoints:

- User Registration (POST): http://localhost:3000/api/v1/users

        {
        	"fullName":"FullName",
        	"email":"example@example.com",
        	"password":"Pa%%wOrd",
        	"userName":"UserName"
        } 
        
   Validations:
   
    - fullName: at least 15 characters
     
    - email: valid email 
    
    - password: between 8 to 25
    
    - userName: at least 5 characters
    
  Response :  status code 200
        
         "data": {
                "user": {
                    "id": xxxxx,
                    "fullName": "XXXXX",
                    "password": "XXXXXX",
                    "email": "XXXXXX",
                    "userName": "XXXXX",
                    "updatedAt": "XXXXXXX",
                    "createdAt": "XXXXXXX",
                    "jwt": "JWT TOKEN SAVED IN DB"
                },
                "token": "JWTTOKEN"
            }
  Error:  status code 400
          
          {
              "message": "Error Message"
          }
        
        
___
- User Login (POST) : http://localhost:3000/api/v1/users/login




          {
            "email":"a@a.com",
            "password":"Os@12345"
          }
      
   Response: status code 200
   
      "data": {
              "user": {
                  "id": XXXXX,
                  "fullName": "XXXXXXX",
                  "password": "XXXXXXXX",
                  "email": "XXXXXXX",
                  "userName": "XXXXXXX",
                  "jwt": "JWT IN DB",
                  "createdAt": "2019-08-21T19:18:14.000Z",
                  "updatedAt": "2019-08-22T13:59:27.231Z"
              },
              "token": "JWT"
          }
          
   Error: status code 400
        
        {
            "message": "Wrong Credentials"
        }
        
---
- Authenticated User Info(GET): http://localhost:3000/api/v1/users/me


  Response :  status code 200
        
         "data": {
                 "fullName": "Ahmed Ibrahim",
                 "email": "a@a.com",
                 "userName": "Ahmedhemaz"
             }
  Error:  status code 400
          
          {
              "message": "Error Message"
          }
        
        
___
- Authenticated User Notes(GET): http://localhost:3000/api/v1/users/:id/notes

 - if id !== id of authenticated user will get public notes only


        
        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }

  Response :  status code 200
        
        
             "data": {
                 "notes": [
                     {
                         "id": X,
                         "title": "XXXXXXXXXXXXXXX",
                         "content": "XXXXXXXXXX",
                         "image": "XXXXXXXXXX",
                         "public": XXXX,
                         "createdAt": "XXXXXXXXXX",
                         "updatedAt": "XXXXXXXXXX",
                         "userId": X
                     },
                     {
                         "id": X,
                         "title": "XXXXXXXX",
                         "content": "XXXXXXXXXXXXXXXXXXXxx",
                         "image": "XXXXXXXXXXXXXXXxxx",
                         "public": XXXX,
                         "createdAt": "XXXXXXXXX",
                         "updatedAt": "XXXXXXXXX",
                         "userId": X
                     }
                 ]
             }
  Error:  status code 400
          
          {
              "message": "Error Message"
          }
        
        
___
- Update Authenticated User(PUT): http://localhost:3000/api/v1/users/settings


   Request-Header:
   
        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }
        
  Request-Body: 
        
        	{
        	    "fullName":"XXXXXX",
        	    "email":"email",
        	    "password":"password",
        	    "userName":"userName"
        	}
        
        
  Validations:
     
   - fullName: at least 15 characters
       
   - email: valid email 
      
   - password: between 8 to 25 (if old password was entered will throw an exception)
      
   - userName: at least 5 characters
     
   - If there is no data at any field will keep the old data


   Response: status code 200
   
      "data": {
              "user": {
                  "id": XXXXX,
                  "fullName": "XXXXXXX",
                  "password": "XXXXXXXX",
                  "email": "XXXXXXX",
                  "userName": "XXXXXXX",
                  "jwt": "JWT IN DB",
                  "createdAt": "2019-08-21T19:18:14.000Z",
                  "updatedAt": "2019-08-22T13:59:27.231Z"
              },
              "token": "JWT"
          }
  Error:  status code 400/401
          
          {
              "message": "Error Message"
          }
___
- Logout Authenticated User(POST): http://localhost:3000/api/v1/users/logout


        
        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }

  Response :  status code 200
        
        "message":  "logged out successfully" 
  Error:  status code 400
          
          {
              "message": "Error Message"
          }
        
        
___
- Create Note(POST): http://localhost:3000/api/v1/notes


        
        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }
        
  Request-Body: use form-data (Postman)
        
        	{
                "title":"Title",
                "content":"content",
                "image":"Upload image",
                "public":"true or false"
            }
        
        
  Validations:
         
   - title: at least 5 characters
     
   - content: between 25 and 255
    
   - image: (.png, .jpg, .jpeg) and size 0~5 MB (optional)
    
   - public: (true or false) -> default (true)
    


  Response :  status code 200
        
        
        "data": {
                "note": {
                    "id": X,
                    "title": "XXXXXXX",
                    "content": "XXXXXXXXXXXXXXX",
                    "image": "XXXXXXXXXXXXXXXXx",
                    "userId": X,
                    "public": XXXXXX,
                    "updatedAt": "XXXXXXXX",
                    "createdAt": "XXXXXXXX"
                }
            }
  Error:  status code 400
          
          {
              "message": "Error Message"
          }
___
- Show Note By Id(GET): http://localhost:3000/api/v1/notes/id


        
        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }

  Response :  status code 200
        
        
         "data": {
                 "note": {
                     "id": 2,
                     "title": "noteupdate245",
                     "content": "noteupdate22asdfasdfsadfsdafsdafasdf",
                     "image": "public/images/notes/1566426299993-5-50888_products-naruto-jiraiya-keychain.png",
                     "public": true,
                     "createdAt": "2019-08-21T22:25:00.000Z",
                     "updatedAt": "2019-08-22T00:15:48.000Z",
                     "userId": 1
                 }
             }   
  Error:  status code 400/401/404
          
          {
              "message": "Error Message"
          }
        
        
___
- Update Note(PUT): http://localhost:3000/api/v1/notes/id


        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }
        
  Request-Body: use form-data (Postman)
        
        	{
        	    "title":"Title",
        	    "content":"content",
        	    "image":"Upload image",
        	    "public":"true or false"
        	}
        
        
  Validations:     
         
   - title: at least 5 characters
     
   - content: between 25 and 255
    
   - image: (.png, .jpg, .jpeg) and size 0~5 MB
    
   - public: (true or false) -> default (true)
    
   - If there is no data at any field will keep the old data


  Response :  status code 200
        
        
        "data": {
                "note": {
                    "id": X,
                    "title": "XXXXXXX",
                    "content": "XXXXXXXXXXXXXXX",
                    "image": "XXXXXXXXXXXXXXXXx",
                    "userId": X,
                    "public": XXXXXX,
                    "updatedAt": "XXXXXXXX",
                    "createdAt": "XXXXXXXX"
                },
                "message": "resource updated successfully"
            }
  Error:  status code 400/401/404
          
          {
              "message": "Error Message"
          }
___
- Delete Note By Id(DELETE): http://localhost:3000/api/v1/notes/id


        
        Headers:{
            "Authorization":"Bearer JWT"
            "Content-Type":"application/json"
        }

  Response :  status code 200
        
        "message":  "resource deleted successfully" 
  Error:  status code 400/401/404
          
          {
              "message": "Error Message"
          }
        
        
___
- Get Note Image(GET): http://localhost:3000/api/v1/notes/id/image



  Response :  status code 200
        
        the image 
  Error:  status code 400/404
          
          {
              "message": "Error Message"
          }
___
