import { Component, OnInit } from '@angular/core';
import {NoteService} from "../note-service";
import {NoteModel} from "../models/note.model";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-notes-info',
  templateUrl: './notes-info.component.html',
  styleUrls: ['./notes-info.component.scss'],
  providers: [ NoteModel ]
})
export class NotesInfoComponent implements OnInit {
  errorMessage:string;

  constructor(private  noteService:NoteService, private activatedRoute:ActivatedRoute,private note:NoteModel,
              private authService:AuthService, private router:Router) { }

  ngOnInit() {
    this.noteService.getNoteIdFromParams(this.activatedRoute);
    this.noteService.fetchNoteById(this.noteService.noteId)
      .subscribe(
        (note:any)=>{
          this.note = note.data.note;
        },
        error => this.errorMessage = error.error.message
      );
  }

  onDelete(noteId:number){
    this.noteService.deleteNote(noteId)
      .subscribe(
        (data:any)=>{
          console.log(data);
          this.router.navigate(['/users',this.authService.currentUser.id,'notes'])
        },
        error => console.log(error)
      )
  }

}
