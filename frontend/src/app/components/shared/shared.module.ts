import {NgModule} from "@angular/core";


import {InputFieldComponent} from "./input-field/input-field.component";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    InputFieldComponent,
  ],
  imports:[
    ReactiveFormsModule
  ],
  exports: [
    InputFieldComponent,
  ]
})
export class SharedModule {

}
